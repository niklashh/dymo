#!/bin/sh
[ -z "$1" ] && { echo "Usage: $0 <NEW FILENAME>"; exit 1; }
ls -1 template.* |sed -E "s/^template(.*)\$/template\1 $1\1/g" |xargs -L1 cp -v
