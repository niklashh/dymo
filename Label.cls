\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Label}[2022/09/11 Dymo LabelWriter label]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}
\ProcessOptions\relax

\LoadClass[8pt]{extarticle}

\usepackage{array} % Required for manipulating table columns

\setlength{\parindent}{0pt} % No paragraph indentation

\pagestyle{empty} % Suppress headers and footers

\makeatletter
\let\@minipagerestore=\raggedright % Ensure all text in minipages is left aligned without hyphenation
\makeatother

\usepackage[
	top=0.4cm,
	bottom=0.25cm,
	left=0.25cm,
	right=0.25cm,
]{geometry}

\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage[utf8]{inputenc} % Required for inputting international characters

\usepackage[
	default,
	oldstyle,
	light,
	semibold,
]{raleway}

\usepackage{microtype} % Improve typography
