#!/bin/sh
[ -z "$1" ] && { echo "Usage: $0 <TEXT TO PRINT>"; exit 1; }

cat <<-EOF > /dev/shm/quickprint.tex
\documentclass{Label}
\geometry{paperwidth=43mm, paperheight=12mm}

\begin{document}

\vspace*{-0.5cm}
\hspace*{-0.2cm}
\minipage[c]{3cm} % Left column width
         {\fontsize{15pt}{15pt}\selectfont $1}
\endminipage

\end{document}
EOF

pdflatex -output-directory /dev/shm /dev/shm/quickprint.tex

lp -d "DYMO_LabelWriter_330_Turbo" -o landscape -o PageSize=w41h144 /dev/shm/quickprint.pdf

rm /dev/shm/quickprint.{tex,log,aux}

